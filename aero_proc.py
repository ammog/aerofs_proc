#!/usr/bin/env python 
# CursesExample1
#------------------------------- 
# Curses Programming Sample 1 
#------------------------------- 
import curses 
import sys
import os
import sys

tdw_x = 2 #total download x
tdw_y = 4 #total download y

tup_x = 40 #total upload x
tup_y = 4 #total upload y

tst_x = 2 #total status x
tst_y = 0 #total status y

lln_x = 1 #last line
lln_y = 1 #last line

upd_hdr = "LAST UPDATE: "
dw_hdr = "DOWNLOADING: "
up_hdr = "UPLOADING: "
lln_hdr = "LINE: "

aerofs_path = '/home/AeroFS/'

tstatus_pos = {'y':tst_y,'x':(tst_x+len(upd_hdr))}
tdwstat_pos = {'y':tdw_y,'x':(tdw_x+len(dw_hdr))} 
tupstat_pos = {'y':tup_y,'x':(tup_x+len(up_hdr))} 
lastlin_pos = {'y':lln_y,'x':(lln_x+len(lln_hdr))} 
proc_dw = {}
proc_up = {}
Kb = 1024
Mb = Kb*Kb
Gb = Mb*Kb
def showSize(b):
	if b == 0: return '0'
	if b > Gb: return '%.1fGb'%(float(b)/Gb) 
	if b > Mb: return '%.1fMb'%(float(b)/Mb) 
	if b > Kb: return '%.1fKb'%(float(b)/Kb) 
	return '%dB'%b


try: 
	myscreen = curses.initscr() 
	myscreen.clear()
	myscreen.border(0)
	myscreen.addstr(lastlin_pos['y'], lln_x, lln_hdr) 
	#myscreen.addstr(tdwstat_pos['y'], tdw_x, dw_hdr) 
	myscreen.addstr(tupstat_pos['y'], tup_x, up_hdr) 
	myscreen.addstr(tstatus_pos['y'], tst_x, upd_hdr) 
	myscreen.refresh()


	ln_win = curses.newwin(3, 78, lln_y, lln_x)
	#ln_win.box()
	ln_win.addstr(0, 1, lln_hdr)
	ln_win.refresh()

	dw_win = curses.newwin(19, 37, tdw_y, tdw_x)
	dw_win.box()
	dw_win.addstr(0, 1, dw_hdr)
	dw_win.refresh()

	up_win = curses.newwin(19, 37, tup_y, tup_x)
	up_win.box()
	up_win.addstr(0, 1, up_hdr)
	up_win.refresh()


	def printProgress(caption, win, dct):
		win.clear()
		win.box()
		#(x, y) = (tdw_x,tdw_y+1)
		(x, y) = (1,1)
		cnt = 0
		total_size = 0.0
		
		finished = 0.0
		for k,v in dct.iteritems():
			b = v['size']
			p = int(v['proc'][:-1])
			cnt += 1
			total_size += b
			finished += b*p/100.0
			win.addstr(y, x, "{0} - {1}%".format(showSize(b),p))
			y += 1
			if y>=19:
				 (x, y) = (x+12,1)
			if x>30: break
		total_proc = 0.0
		if total_size != 0: total_proc = 100.0*float(finished) / total_size		
		win.addstr( 0,1, "{0}: {1}%, {2}/{3}".format(caption,int(total_proc),showSize(finished),showSize(total_size)) )
		win.refresh()
		
			

	while 1:
		try:
			line = sys.stdin.readline()

		except KeyboardInterrupt:
			break

		if not line:
			break
		ln_win.clear()		
		ln_win.addstr( 0,0, "LINE: {0}".format(line) )
		ln_win.refresh()
		p = line.split("|")
		if len(p) == 4:
			(time,direct,proc,name) = (p[0][:-1],p[1][1:-1],p[2][1:-1],p[3][1:-1])
			myscreen.addstr( tstatus_pos['y'],tstatus_pos['x'], time )
			if direct == 'DL': procfiles = proc_dw
			elif direct == 'UL': procfiles = proc_up
			if name == 'metadata': continue
			elif 'ended' in proc: del procfiles[name]
			elif '%' not in proc: continue
			elif '100%' in proc: del procfiles[name]
			else: 
				if name in procfiles:
					procfiles[name]['proc'] = proc
				else:
					path = aerofs_path + name[1:]
					b = 0
					if os.path.isfile(path): b = os.path.getsize(path)
					procfiles[name] = {'proc':proc, 'size':b}
			
			#myscreen.addstr( tdwstat_pos['y'],tdwstat_pos['x'], "{0}".format(len(proc_dw)) )
			#myscreen.addstr( tupstat_pos['y'],tupstat_pos['x'], "{0}".format(len(proc_up)) )

			printProgress("DOWNLOADING", dw_win, proc_dw)
			printProgress("UPLOADING", up_win, proc_up)
		myscreen.refresh() 

	#myscreen.getch() 
finally:
	x = 1 
	curses.endwin()
